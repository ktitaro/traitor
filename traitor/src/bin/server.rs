#[macro_use]
extern crate log;
extern crate traitor;

use std::io::{self, Write};
use std::net::{TcpListener, TcpStream, SocketAddr};
use failure::Fallible;
use traitor::{recv, send, is_client_connected};

fn main() {
    env_logger::init();
    let addr = std::env::var("ADDR").expect("ADDR must be provided");
    run(&addr).expect("Error occured while running TCP server")
}

fn run<'a>(addr: &'a str) -> Fallible<()> {
    let listener = TcpListener::bind(addr)?;

    info!("TCP server is running at {}", addr);
    info!("Press CTRL+C to exit");
    info!("Waiting for client to connect");

    loop {
        if let Ok((stream, addr)) = listener.accept() {
            info!("New client connected {}", addr);
            stream.set_nonblocking(true)?;
            communicate(&stream, &addr)?;
            info!("{} has been disconnected", addr);
            info!("Waiting for client to connect");
        }
    }
}

fn communicate<'a>(stream: &'a TcpStream, addr: &'a SocketAddr) -> Fallible<()> {
    info!("You now communicating with {}", addr);
    info!("Type \"quit\" to exit");

    let mut buffer = String::new();

    loop {
        if let Ok(connected) = is_client_connected(stream) {
            if !connected {
                break Ok(())
            }

            print!("{} $ ", addr);
            io::stdout().flush()?;

            if let Ok(n) = io::stdin().read_line(&mut buffer) {
                if n > 0 {
                    if buffer.starts_with("quit") {
                        break Ok(())
                    }

                    buffer.pop();
                    send(stream, &buffer)?;
                    buffer.clear();

                    let output = recv(stream)?;
                    if output != "" {
                        println!("{}", output);
                    }
                }
            }
        }
    }
}
