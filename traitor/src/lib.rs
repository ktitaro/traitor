use std::io::{self, Write, Read};
use std::net::TcpStream;
use std::time::Duration;
use std::thread::sleep;
use failure::Fallible;

const BUFFER_SIZE: usize = 8192;

pub fn recv<'a>(mut stream: &'a TcpStream) -> Fallible<String> {
    let mut buffer: [u8; BUFFER_SIZE] = [0; BUFFER_SIZE];
    loop {
        match stream.read(&mut buffer[..]) {
            Ok(n) => {
                if n > 0 {
                    let filtered = buffer.iter().filter(|&&x| x != 0).map(|x| *x).collect::<Vec<u8>>();
                    let message = String::from_utf8(filtered)?;
                    break Ok(message.to_string())
                } else {
                    break Ok(String::default())
                }
            },
            Err(e) => {
                if e.kind() == io::ErrorKind::WouldBlock {
                    sleep(Duration::from_millis(100));
                    continue
                } else {
                    break Ok(String::default())
                }
            }
        }
    }
}

pub fn send<'a>(mut stream: &'a TcpStream, message: &'a str) -> Fallible<()> {
    Ok(write!(&mut stream, "{}", message)?)
}

pub fn is_client_connected<'a>(mut stream: &'a TcpStream) -> Fallible<bool> {
    let mut buffer: Vec<u8> = vec![];
    let check = match stream.read_to_end(&mut buffer) {
        Ok(n) if n == 0 => false,
        _ => true,
    };
    Ok(check)
}
